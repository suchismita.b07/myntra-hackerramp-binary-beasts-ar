# Myntra HackerRamp Binary Beasts AR

## Introduction
This is an extension to the main [Myntra HackerRamp Binary Beasts](https://gitlab.com/suchismita.b07/hackerramp-binary-beasts-myntra-21) project. This project implements AR by rendering 3D models in real-world scenes. 

## A-Frame 
The official [documentation](https://aframe.io/docs/1.2.0/introduction/) for A-Frame.

## 8thWall
The official [documentation](https://www.8thwall.com/docs/web/) for 8th Wall.

## Credits for the 3D models
1. [Black Hat](https://skfb.ly/6tZtT)
2. [Cowboy Hat](https://skfb.ly/6WVoG)
3. [Witch Hat](https://skfb.ly/BGNp)
4. [Cactus](https://skfb.ly/6BTMs)
5. [Green Plant](https://skfb.ly/6TX9R)
6. [Potted Plant](https://skfb.ly/6vONo)

